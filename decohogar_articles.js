import xml2js from "https://esm.sh/xml2js?pin=v55&deno-std=0.116.0";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const minPrice = Deno.env.get("MIN_PRICE");
const user = Deno.env.get("HTTP_USER");
const pass = Deno.env.get("HTTP_PASS");
const partial = Deno.env.get("PARTIAL");
const branchMap = {
  204: { empresa: "ABC", codigo: "00000204" },
  205: { empresa: "ABC", codigo: "00000205" },
  209: { empresa: "ABC", codigo: "00000209" },
  211: { empresa: "ABC", codigo: "00000211" },
  Marcal: { empresa: "Marcal", codigo: "Stock disponible" },
  310: { empresa: "MRomero", codigo: "00000310" },
};

async function init() {
  let articleStock = await getRemoteArticleStock();
  let articleData = await getRemoteArticleData();
  await processAllArticles(articleStock, articleData);
}
async function getRemoteArticleStock() {
  let response =
    await makeRequestToDecoHogar(`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsg="http://wsGenQueryFacade/">
  <soapenv:Header/>
  <soapenv:Body>
    <wsg:GetData>
        <wsg:keyCode>WEB_STOCK</wsg:keyCode>
        <wsg:parameters><![CDATA[<params><empresa></empresa><articulo></articulo></params>]]></wsg:parameters>
    </wsg:GetData>
  </soapenv:Body>
</soapenv:Envelope>`);

  return response;
}

async function getRemoteArticleData() {
  let response =
    await makeRequestToDecoHogar(`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsg="http://wsGenQueryFacade/">
  <soapenv:Header/>
  <soapenv:Body>
     <wsg:GetData>
        <wsg:keyCode>WEB_ARTICULOS</wsg:keyCode>
        <wsg:parameters><![CDATA[<params></params>]]></wsg:parameters>
     </wsg:GetData>
  </soapenv:Body>
</soapenv:Envelope>`);

  return response;
}

async function makeRequestToDecoHogar(requestBody) {
  let response = await fetch(
    `https://BITWEB.marcal.intra:7443/WsGenQueryFacade/WsGenQuery.asmx`,
    {
      method: "POST",
      headers: {
        Authorization: "Basic " + btoa(user + ":" + pass),
        "Content-Type": "text/xml;charset=UTF-8",
      },
      body: requestBody,
    }
  );

  response = await response.text();
  response = response.replace(/&lt;/g, "<");
  response = response.replace(/&gt;/g, ">");
  let parser = new xml2js.Parser();
  let parsedResponse = await parser.parseStringPromise(response);

  console.log(parsedResponse["soap:Envelope"]["soap:Body"][0]["GetDataResponse"][0][
    "GetDataResult"
  ][0]["NewDataSet"][0]["Table"]);

  return parsedResponse["soap:Envelope"]["soap:Body"][0]["GetDataResponse"][0][
    "GetDataResult"
  ][0]["NewDataSet"][0]["Table"];
}

async function processAllArticles(articleStock, articleData) {
  let stockMap = {};
  let articlesToSend = [];

  for (let stock of articleStock) {
    if (!stockMap[stock["Codigo_Articulo"][0]]) {
      stockMap[stock["Codigo_Articulo"][0]] = {};
    }
    if (!stockMap[stock["Codigo_Articulo"][0]][stock["Empresa"][0]]) {
      stockMap[stock["Codigo_Articulo"][0]][stock["Empresa"][0]] = [];
    }
    stockMap[stock["Codigo_Articulo"][0]][stock["Empresa"][0]].push(stock);
  }

  for (let article of articleData) {
    try {
      if (article["Lista"][0] != "0") {
        if (Number(article["Lista304"][0]) >= minPrice) {
          let processedArticle = await processArticle(stockMap, article);
          articlesToSend.push(processedArticle);
        } else {
          console.log(
            `Article ${article.Codigo} price ${article.Lista304} is too low to publish`
          );
        }
      } else {
        console.log(`Article ${article.Codigo} requirements aren't met`);
      }
    } catch (e) {
      console.log(`Failed to process article: ${article}, ${e.message}`);
    }
  }

  try {
    await sagalDispatchBatch({
      products: articlesToSend,
      batch_size: 100,
    });
  } catch (e) {
    console.log(`Failed to send articles, ${e.message}`);
  }
}

async function processArticle(stockMap, article) {
  let articleSku = article["Codigo"][0];
  let articleName =
    article["Descripcion_Web"][0] && article["Descripcion_Web"][0] != ""
      ? article["Descripcion_Web"][0]
      : article["Descripcion"][0];
  let articleDescription =
    article["Descripcion_Web"][0] && article["Descripcion_Web"][0] != ""
      ? article["Descripcion_Web"][0]
      : article["Descripcion"][0];
  let articlePrice = {
    currency: "$",
    value:
      article["Oferta"] && article["Oferta"][0] == "true"
        ? Number(article["Lista304"][0]) * 0.8
        : Number(article["Lista304"][0]),
  };
  let articleStock =
    (await getBranchAmount(stockMap, article, {
      empresa: "Marcal",
      codigo: "Stock disponible",
    })) || 0;
  let minStock =
    stockMap[article["Codigo"][0]] &&
      stockMap[article["Codigo"][0]]["Stock Mínimo"]
      ? Number(stockMap[article["Codigo"][0]]["Stock Mínimo"][0]?.Cantidad[0])
      : 0;
  articleStock = articleStock + minStock >= 0 ? articleStock + minStock : 0;
  let articleBranchStock = {};
  for (let [branchName, branchData] of Object.entries(branchMap)) {
    let branchStock =
      (await getBranchAmount(stockMap, article, branchData)) || 0;
    articleBranchStock[branchName] = {
      value: branchStock || 1,
      availability: 48,
    };
  }
  let articleAttributes = {};
  if (article["W_Marca"]) {
    articleAttributes = {
      Marca: article["W_Marca"][0] || "NONE",
    };
  }

  let processedArticle = {
    sku: articleSku.trim(),
    client_id: clientId,
    options: {
      merge: false,
      partial: partial === "true",
    },
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      let ecommerceProps = {
        ecommerce_id: ecommerce_id,
        properties: [
          { name: articleName },
          { description: articleDescription },
          {
            price: articlePrice,
          },
          {
            stock: articleStock,
          },
          {
            storestock: articleBranchStock,
          },
          {
            attributes: articleAttributes,
          },
        ],
        variants: [],
      };
      return ecommerceProps;
    }),
  };

  return processedArticle;
}

async function getBranchAmount(stockMap, article, branchData) {
  try {
    let branchAmount = Number(
      stockMap[article.Codigo[0]][branchData.empresa].find(
        (value) => value.Codigo_Deposito[0] == branchData.codigo
      )?.Cantidad[0]
    );
    if (branchAmount) {
      return branchAmount;
    } else {
      return 0;
    }
  } catch (e) {
    console.log(`WARN: returning 0 stock for ${article.Codigo[0]}`);
    return 0;
  }
}

await init();
