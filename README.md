### Integration script for client: Districomp

## Introduction

These scripts were created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Decohogar is a client that mainly sends their data through a SOAP API, exposing endpoints that Sagal can request from.

## Workflow (Article price and stock)

- Article price and stock data is retrieved through two separate requests.
- The data is restructured and processed for each article, being transformed to a Sagal-friendly format.
- The restructured data is dispatched to Sagal.

## Workflow (Article images)

-Article image data is retrieved from the client's folder in google drive.
-The images are uploaded to Amazon S3 and the resulting URLs assigned to their respective article.
-The image urls are dispatched to Sagal.
