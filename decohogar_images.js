import { googleDriveAccess } from "https://bitbucket.org/sagal/gdrive_js/raw/master/google_drive_access.js";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const integratorApiUrl = Deno.env.get("INTEGRATOR_API");
const googleClientId = Deno.env.get("GOOGLE_CLIENT_ID");
const googleSecret = Deno.env.get("GOOGLE_SECRET");
const refreshToken = Deno.env.get("GOOGLE_REFRESH_TOKEN");
const imagesDirectory = Deno.env.get("IMAGES_DIRECTORY");
const imgUploadFolder = Deno.env.get("IMAGE_UPLOAD_FOLDER");
const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID");
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY");
const awsRegion = Deno.env.get("AWS_REGION");
const partial = Deno.env.get("PARTIAL");

const bucket = new S3Bucket({
  accessKeyID: awsKeyId,
  secretKey: awsSecretKey,
  bucket: "sagal-excel-uploads-0181239103940129",
  region: awsRegion,
});

let amazonImageSizeMap = {};

async function init() {
  let allArticleSkus = await getExistingArticles();
  await mapAllImagesOnS3ForClient();
  for (let articleSku of allArticleSkus) {
    if (articleSku) {
      let accessToken = await getAccessToken();
      try {
        await processAllImagesForArticle(articleSku, accessToken);
      } catch (e) {
        console.log(`Unable to process images for article: ${e.message}`);
      }
    }
  }
}

async function mapAllImagesOnS3ForClient() {
  const allImagesOnS3ForClient = bucket.listAllObjects({
    delimiter: `/`,
    prefix: `${imgUploadFolder}/`,
  });

  for await (let imageOnS3 of allImagesOnS3ForClient) {
    amazonImageSizeMap[imageOnS3.key.split("/")[1].toLowerCase()] = {
      name: imageOnS3.key.split("/")[1].toLowerCase(),
      size: imageOnS3.size,
    };
  }
}

async function getExistingArticles() {
  let articleSkus = [];
  for (let ecommerceId of Object.values(clientEcommerce)) {
    let response = await fetch(
      `${integratorApiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`
    );
    let allArticleSkusAsKeys = await response.json();

    for (let articleSku of Object.keys(allArticleSkusAsKeys)) {
      if (!articleSkus.find((x) => x == articleSku)) {
        articleSkus.push(articleSku);
      }
    }
  }

  return articleSkus;
}

async function sendImagesToSagal(articleSku, images) {
  let articleImages = images;

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    options: {
      merge: false,
      partial: partial === "true",
    },
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      let ecommerceProps = {
        ecommerce_id: ecommerce_id,
        options: {
          override_create: {
            send: false,
          },
        },
        properties: [
          {
            images: articleImages,
          },
        ],
        variants: [],
      };
      return ecommerceProps;
    }),
  };

  await sagalDispatch(processedArticle);
}

async function getAccessToken() {
  try {
    let accessToken = await googleDriveAccess.getGoogleDriveAccessToken(
      googleClientId,
      googleSecret,
      refreshToken
    );
    return accessToken;
  } catch (e) {
    console.log(`Authorization failed: ${e.message}`);
  }
}

async function processAllImagesForArticle(articleSku, accessToken) {
  try {
    let patternA = `${
      articleSku.includes("/") ? articleSku.replace(/\//g, "B") : articleSku
    }`;
    let patternB = `${
      articleSku.includes("/") ? articleSku.replace(/\//g, "-") : articleSku
    }`;

    let imagesInArticle = [];

    let allImageFileInfo = await googleDriveAccess.getAllFilesFromFolder(
      imagesDirectory,
      accessToken,
      {
        queryData: [
          `name contains '${patternA}' or name contains '${patternB}'`,
        ],
        isPagedRequest: true,
      }
    );
    allImageFileInfo = allImageFileInfo.filter((imageFileInfo) => {
      let imageName = imageFileInfo.name;
      let isJpgOfArticleA = new RegExp(`${patternA}((_(.*))?).jpg`, "gm");
      let isJpgOfArticleB = new RegExp(`${patternB}((_(.*))?).jpg`, "gm");
      let isPngOfArticleA = new RegExp(`${patternA}((_(.*))?).png`, "gm");
      let isPngOfArticleB = new RegExp(`${patternB}((_(.*))?).png`, "gm");
      return (
        imageName.match(isJpgOfArticleA) ||
        imageName.match(isJpgOfArticleB) ||
        imageName.match(isPngOfArticleA) ||
        imageName.match(isPngOfArticleB)
      );
    });

    let buffer = [];
    for (let imageFileInfo of allImageFileInfo) {
      try {
        let batchImageProcessing = async (imageInfoInBuffer) => {
          console.log(`Retrieving image ${imageInfoInBuffer.name}`);

          let imageData = await googleDriveAccess.getSpecificFileContentById(
            imageInfoInBuffer.id,
            accessToken
          );
          imageData = await imageData.blob();
          let imageBuffer = await imageData.arrayBuffer();
          imageData = new Uint8Array(imageBuffer);

          if (await imageHasNotBeenModified(imageInfoInBuffer)) {
            console.log(`Image ${imageInfoInBuffer.name} already uploaded`);
            let imageUrl = await getImageURI(imageInfoInBuffer.name);
            imagesInArticle.push(imageUrl);
          } else {
            let imageUrl = await uploadImageAndGetURI(
              imageInfoInBuffer.name,
              imageData
            );
            if (imageUrl) {
              imagesInArticle.push(imageUrl);
            }
          }
        };

        if (buffer.length < 2) {
          buffer.push(imageFileInfo);
        }

        if (buffer.length == 2) {
          await Promise.all(buffer.map(batchImageProcessing));
          buffer = [];
        }
      } catch (e) {
        console.log(
          `Unable to retrieve image ${imageFileInfo.name}: ${e.message}`
        );
      }
    }
    if (buffer.length) {
      let batchImageProcessing = async (imageInfoInBuffer) => {
        console.log(`Retrieving image ${imageInfoInBuffer.name}`);

        let imageData = await googleDriveAccess.getSpecificFileContentById(
          imageInfoInBuffer.id,
          accessToken
        );
        imageData = await imageData.blob();
        let imageBuffer = await imageData.arrayBuffer();
        imageData = new Uint8Array(imageBuffer);

        if (await imageHasNotBeenModified(imageInfoInBuffer)) {
          console.log(`Image ${imageInfoInBuffer.name} already uploaded`);
          let imageUrl = await getImageURI(imageInfoInBuffer.name);
          imagesInArticle.push(imageUrl);
        } else {
          let imageUrl = await uploadImageAndGetURI(
            imageInfoInBuffer.name,
            imageData
          );
          if (imageUrl) {
            imagesInArticle.push(imageUrl);
          }
        }
      };

      try {
        await Promise.all(buffer.map(batchImageProcessing));
        buffer = [];
      } catch (e) {
        console.log(`Unable to upload image batch: ${e.message}`);
      }
    }
    if (imagesInArticle) {
      await sendImagesToSagal(articleSku, imagesInArticle.sort());
    }
  } catch (e) {
    throw new Error(
      `Failed to process images in article ${articleSku}: ${e.message}`
    );
  }
}

async function imageHasNotBeenModified(imageInfo) {
  if (amazonImageSizeMap[imageInfo.name.toLowerCase()]) {
    return (
      amazonImageSizeMap[imageInfo.name.toLowerCase()].size == imageInfo.size
    );
  } else {
    return false;
  }
}

async function uploadImageAndGetURI(imageName, imageData) {
  try {
    await bucket.putObject(
      `/${imgUploadFolder}/${imageName.toLowerCase()}`,
      imageData,
      {
        contentType: "image/jpeg",
        acl: "public-read",
      }
    );
    console.log(
      `Image uploaded: https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/${imgUploadFolder}/${imageName.toLowerCase()}`
    );
    return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/${imgUploadFolder}/${imageName.toLowerCase()}`;
  } catch (e) {
    console.log(`Unable to upload image: ${e.message}`);
  }
}

async function getImageURI(imageName) {
  return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/${imgUploadFolder}/${imageName.toLowerCase()}`;
}

await init();
